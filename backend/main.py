# -*- coding: utf-8 -*-

from flask import Flask, render_template, flash, request
import boto3
import os

# App config.
DEBUG = True
SNS_TOPIC_ARN = os.environ['SNS_ARN']

app = Flask(__name__)
app.config.from_object(__name__)


def send_sns(data):
    ''' 
    https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/sns.html#client 
    '''
    client = boto3.client('sns')
    pass

@app.route('/api/send_email_with_sns', methods=['GET', 'POST'])
def send_message():
    content = request.get_json(force=True)
    send_sns(content)

if __name__ == "__main__":
    app.run()
